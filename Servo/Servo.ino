int t = 0;
Servo servo;

void setup()
{
  servo.attach(A4);
}

void loop()
{
  servo.write(abs(90 - t));
  t++;
  if (t == 180)
  {
    t = 0;
  }
}
