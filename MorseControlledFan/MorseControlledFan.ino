// MorseControlledFan.ino
//
// Reads a series of dots and dashes from button presses into a buffer.
// When the buffer contains a recognizable sequence of dots and dashes,
// that sequence is turned into a number, which in turn sets the speed
// of the fan.
//
// Author: Kelvin Jin (kkj9)

const int fanPin = D0;
const int buttonPin = A0;

// Morse code sequences for 0-9 respectively
const char SEQUENCE_LENGTH = 5;
const char morseLookup[10][8] =
{
  "-----", ".----", "..---", "...--", "....-",
  ".....", "-....", "--...", "---..", "----."
};
const char DASH = '-'; // Letter that represents a dash
const char DOT = '.'; // Letter that represents a dot

void setup()
{
  pinMode(fanPin, OUTPUT);
}

void loop()
{
  readSerialInput();
  readButtonInput(buttonPin);
  if (bufferCanPop())
  {
    char readValue = bufferPeek();
    if (readValue < '0' || readValue > '9')
    {
      return;
    }
    // It's safe to pop this from the buffer
    bufferPop();
    double power = (readValue - '0') / 9.0;
    int fanSpeed = (int)(power * 255);
    analogWrite(fanPin, fanSpeed);
    Serial.print(" ");
    Serial.println(fanSpeed);
  }
}

// For debugging purposes - simply reads from serial monitor
// and updates the state
void readSerialInput()
{
  while (Serial.available())
  {
    // Read a single inputted character
    if (bufferCanPush())
    {
      bufferPush(Serial.read());
    }
  }
}

/* Reading Button Input */

int currButtonState = LOW;
int prevButtonState = LOW;

// Length of a dash in ms
const int dashLength = 300;
// length of a dot in ms
const int dotLength = 100;
// start of a pressed event
unsigned long int startHigh;
// end of a pressed event
unsigned long int stopHigh;
// start of a non-pressed event
unsigned long int startLow;
// end of a non-pressed event
unsigned long int stopLow;

// Given a pin, updates the state of the sketch
void readButtonInput(byte pin)
{
  prevButtonState = currButtonState;
  // observed the state of the button
  byte newButtonState = digitalRead(pin);
  // There is a LOW -> HIGH transition
  // or a HIGH -> LOW transition
  if (currButtonState != newButtonState)
  {
    // wait for a while (15ms)
    delay(15);
    // update the state of the button
    newButtonState = digitalRead(pin);
  }
  currButtonState = newButtonState;
  /* Dash/dot detector */
  // LOW -> HIGH transition
  if ((prevButtonState == LOW) && (currButtonState == HIGH))
  {
    // Update timing
    startHigh = millis();
    stopLow = startHigh;
  }
  // HIGH -> LOW transition
  if ((prevButtonState == HIGH) && (currButtonState == LOW))
  {
    // Update timing
    stopHigh = millis();
    startLow = stopHigh;
    // detect dashs and dots
    if ((stopHigh - startHigh) >= dashLength)
    {
      bufferPush('-');
      Serial.print('-');
    }
    else
    {
      if ((stopHigh - startHigh) >= dotLength)
      {
        bufferPush('.');
        Serial.print('.');
      }
    }
  }
}

/* Functions for manipulating the circular buffer */

// Input abstraction
const int CIRCULAR_BUFFER_LENGTH = 256;
char circularBuffer[CIRCULAR_BUFFER_LENGTH];
int start = 0;
int end = 0;

// Returns whether the circular buffer can accept another character
bool bufferCanPush()
{
  return end != start - 1 && !(start == 0 && end == CIRCULAR_BUFFER_LENGTH - 1);
}

// Gets the length of the circular buffer.
int bufferLength()
{
  if (end >= start)
  {
    return end - start;
  }
  else
  {
    return end + CIRCULAR_BUFFER_LENGTH - start;
  }
}

// Gets the character at a given position in a circular buffer.
char bufferCharAt(int index)
{
  return circularBuffer[(CIRCULAR_BUFFER_LENGTH + start + index) % CIRCULAR_BUFFER_LENGTH];
}

// Pushes a character into the circular buffer
// Does nothing if it's not possible
void bufferPush(char input)
{
  if (bufferCanPush())
  {
    circularBuffer[end++] = input;
    if (end == CIRCULAR_BUFFER_LENGTH)
    {
      end = 0;
    }
    // Collapses a set of dashes and dots
    // if there are at least five such characters
    bufferCollapseMorseCodeSequence();
  }
}

// If the buffer has at least five dots/dashes, try to collapse them
// into a number if the sequence of dots and dashes is known
void bufferCollapseMorseCodeSequence()
{
  // If there are at least five sequence characters in our buffer...
  if (bufferLength() >= 5)
  {
    // Check the last five characters against known sequences.
    int number;
    for (number = 0; number < 10; number++)
    {
      // (The inner loop here simply does this check.)
      int sequenceIndex;
      for (sequenceIndex = 0; sequenceIndex < SEQUENCE_LENGTH; sequenceIndex++)
      {
        char currChar = bufferCharAt(bufferLength() - 5 + sequenceIndex);
        if (morseLookup[number][sequenceIndex] != currChar)
        {
          break;
        }
      }
      if (sequenceIndex == SEQUENCE_LENGTH)
      {
        // We've found a match.
        break;
      }
    }
    // If we found a match, reset the buffer and push just that number.
    if (number != 10)
    {
      start = end;
      bufferPush(number + '0');
    }
  }
}

// Returns whether a character can be popped from the circular buffer
bool bufferCanPop()
{
  return start != end;
}

// Peeks a character frm the circular buffer
// Returns zero if it's not possible
char bufferPeek()
{
  if (bufferCanPop())
  {
    char output = circularBuffer[start];
    return output;
  }
  return 0;
}

// Pops a character frm the circular buffer
// Returns zero if it's not possible
char bufferPop()
{
  if (bufferCanPop())
  {
    char output = circularBuffer[start++];
    if (start == CIRCULAR_BUFFER_LENGTH)
    {
      start = 0;
    }
    return output;
  }
  return 0;
}

// Prints the contents of the circular buffer.
void bufferPrintContents()
{
  int length = bufferLength();
  for (int i = 0; i < length; i++)
  {
    Serial.print(bufferCharAt(i));
  }
  Serial.println();
}
