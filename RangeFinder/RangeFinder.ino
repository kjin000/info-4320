// RangeFinder.ino
//
// Lights up some LED's and makes a speaker play noise depending on how far
// you are away from an expected distance.
// Twisting the potentiometer changes that distance.
//
// Author: Kelvin Jin (kkj9)

#include "math.h"

// Just some fun stuff
const int SCALE_LENGTH = 8;
const float notes[SCALE_LENGTH] = {27.5, 30.87, 16.35, 18.35, 20.60, 21.83, 24.5};
const int SEQUENCE_LENGTH = 6;
const char* sequence[SEQUENCE_LENGTH] =
{
  "C4", "E4", "G4", "C5", "G4", "E4"
};

const int POTENTIOMETER_PIN = A0;
const int RANGE_MODULE_PIN = A1;
const int SPEAKER_PIN = D0;
const int RED_LED_PIN = D1;
const int GREEN_LED_PIN = D2;

const int BEEP_DURATION = 100;
const int SILENCE_DURATION = 100;
const int DISTANCE_THRESHOLD = 200;

int elapsedTime = 0;
int noteNum = 0;

void setup()
{
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
}

// Helper function that turns, for example, A4 -> 440Hz
int frequencyFromNote(const char* note)
{
  if (strlen(note) < 2)
  {
    return 0;
  }
  float base = notes[note[0] - 'A'];
  int octave = note[1] - '0';
  return (int)(base * (1 << octave));
}

void loop()
{
  int expectedDistance, actualDistance, redBrightness, greenBrightness;
  // Read potentiometer and range module
  expectedDistance = analogRead(POTENTIOMETER_PIN);
  actualDistance = analogRead(RANGE_MODULE_PIN);
  int diff = actualDistance - expectedDistance;
  // Set brightness based on that distance
  redBrightness = max(diff / 16, 0);
  greenBrightness = max(-diff / 16, 0);
  // If distance is in threshold, turn on the speaker
  if (abs(diff) < DISTANCE_THRESHOLD)
  {
    tone(SPEAKER_PIN, frequencyFromNote(sequence[noteNum]), BEEP_DURATION);
    delay(BEEP_DURATION + SILENCE_DURATION);
    noteNum++;
    if (noteNum == SEQUENCE_LENGTH)
    {
      noteNum = 0;
    }
  }
  else
  {
    noteNum = 0;
  }
  // Set brightness
  analogWrite(RED_LED_PIN, redBrightness);
  analogWrite(GREEN_LED_PIN, greenBrightness);
}
