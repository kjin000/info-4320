// DrawingArm.ino - Controls Theta/R motors for drawing arm
// You need to be connected in the serial monitor.
// First it will print "When calibrated, press enter: "
// To calibrate, set rotation of arm to the middle (so it splits the paper in
// half) and the pen fixture as close to the tower as possible.
// Then you can press enter.
// Afterwards it will ask for x and y coordinates in inches.
// Assuming 8.5 x 11 paper, x range is (-5.5, 5.5) while y range is (0, 8.5).

#include "Stepper.h"
#include <math.h>

float p1[2];
float p2[2];

const int NUM_STEPS = 50;
const int DELAY_MS = 0;
const float PAPER_WIDTH_INCHES = 11;
const float PAPER_HEIGHT_INCHES = 8.5;
const int R_STEPPER_SPEED = 20;
const int THETA_STEPPER_SPEED = 3;
const float R_OFFSET = 3.5;
const float R_ADJUSTMENT = 4.0/3.7;

int JOYSTICK_CLICK = A0;
int JOYSTICK_HORIZONTAL = A1;
int JOYSTICK_VERTICAL = A2;

const int CALIBRATION_MODE = 0;
const int ENTRY_MODE = 1;
const int RUNNING_MODE = 2;

int mode;
float t;
float prevTheta;
float prevR;
float stepSize = 1.0 / NUM_STEPS;
bool prevJoystickClicked = false;

Stepper stepperR(200, D0, D1, D2, D3);
Stepper stepperTheta(200, D4, D5, D6, D7);

void setup()
{
  mode = CALIBRATION_MODE;
  Serial.setTimeout(60000);
  stepperR.setSpeed(R_STEPPER_SPEED);
  stepperTheta.setSpeed(THETA_STEPPER_SPEED);
  while (!Serial);
  Serial.println("Entering calibration mode... ");
}

void loop()
{
  if (mode == CALIBRATION_MODE)
  {
    stepperR.step(-getJoystickY());
    stepperTheta.step(getJoystickX());
    if (getJoystickClicked())
    {
      mode = ENTRY_MODE;
    }
  }
  if (mode == ENTRY_MODE)
  {
    while (true)
    {
      Serial.print("\nEnter 1st point X (inches): ");
      p1[0] = acceptFloat();
      Serial.print("\nEnter 1st point Y (inches): ");
      p1[1] = acceptFloat();
      Serial.print("\nEnter 2nd point X (inches): ");
      p2[0] = acceptFloat();
      Serial.print("\nEnter 2nd point Y (inches): ");
      p2[1] = acceptFloat();
      if (validate(p1) && validate(p2))
      {
        Serial.print("\nValidated. Starting...");
        break;
      }
      Serial.print("\nValidation failed.");
    }
    Serial.println();
    prevTheta = 50;
    prevR = R_OFFSET * 200.0 / M_PI * R_ADJUSTMENT;
    t = 0;
    mode = RUNNING_MODE;
  }
  if (mode == RUNNING_MODE)
  {
    printState();
    float x = (1.0 - t) * p1[0] + t * p2[0];
    float y = (1.0 - t) * p1[1] + t * p2[1] + R_OFFSET;
    float theta = atan2(y, x) * 100.0 / M_PI;
    float r = sqrt(x * x + y * y) * 200.0 / M_PI * R_ADJUSTMENT;
    if (abs(r - prevR) >= 1)
    {
      int diff = r - prevR;
      stepperR.step(-diff);
      prevR += diff;
    }
    if (abs(theta - prevTheta) >= 1)
    {
      int diff = theta - prevTheta;
      stepperTheta.step(-diff);
      prevTheta += diff;
    }
    if (t >= 1 || getJoystickClicked())
    {
      mode = CALIBRATION_MODE;
      Serial.println("Entering calibration mode... ");
    }
    else
    {
      t += stepSize;
      delay(DELAY_MS);
    }
  }
}

int getJoystickX()
{
  float amount = -(analogRead(JOYSTICK_HORIZONTAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

float printState()
{
  Serial.print("theta = ");
  Serial.print(prevTheta);
  Serial.print(" r = ");
  Serial.println(prevR);
}

int getJoystickY()
{
  float amount = (analogRead(JOYSTICK_VERTICAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

bool getJoystickClicked()
{
  bool result = analogRead(JOYSTICK_CLICK) < 16 && !prevJoystickClicked;
  prevJoystickClicked = result;
  return result;
}

float acceptFloat()
{
  String s = Serial.readStringUntil('\n');
  Serial.print(s);
  return s.toFloat();
}

bool validate(float* point)
{
  return abs(point[0]) <= PAPER_WIDTH_INCHES / 2.0 && point[1] >= 0 && point[1] < PAPER_HEIGHT_INCHES;
}
