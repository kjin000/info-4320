int SPEED = 51;
int DIRECTION = 1;

// old state of the encoder
boolean old_state = LOW;
// current state of the encoder
boolean current_state = LOW;
// current transition count
int pulse_count;
// number of transition for a full turn
int full_turn_count = 800;

void setup()
{
  // attaches the servo on pin 9 to the servo object
  // myservo.attach(servo_pin);
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
}
void loop()
{
  //Serial.println(analogRead(A1));
  pulse_count = 0;
  setDirection(DIRECTION);
  // loop until we get the correct count
  while (pulse_count < full_turn_count)
  {
    // read the state of the photo-transistor
    current_state = digitalRead(A1);
    if (current_state != old_state)
    {
      // we detected a transition
      pulse_count++;
      // delay here is problem with de-bounding
    }
    old_state = current_state;
  }
  setDirection(0);
  DIRECTION *= -1;
  delay(5000);
}

void setDirection(int direction)
{
  analogWrite(D0, 0);
  if (direction > 0)
  {
    analogWrite(D1, SPEED);
    analogWrite(D2, 0);
  }
  else if (direction < 0)
  {
    analogWrite(D1, 0);
    analogWrite(D2, SPEED);
  }
  else
  {
    analogWrite(D1, 0);
    analogWrite(D2, 0);
  }
  analogWrite(D0, SPEED);
  Serial.println(direction);
}
