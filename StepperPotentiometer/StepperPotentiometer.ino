#include <Stepper.h>
#include "math.h"

int position = 0;
Stepper stepper(200, D0, D1, D2, D3);

void setup()
{
  stepper.setSpeed(30);
}

void loop()
{
  /*int val = analogRead(A3);
  val = map(val, 0, 4095, 0, 200);
  stepper.step(val - position);
  position = val;*/
  for (int i = 0; i < 100; i++)
  {
    int val = abs(49 - i);
    stepper.step(val - position);
    position = val;
  }
}
