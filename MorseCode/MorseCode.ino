// MorseCode.ino
//
// Waits until a user sends a sequence of digits over the serial port.
// Then, displays the morse code sequence corresponding to this sequence
// of digits.
//
// Author: Kelvin Jin (kkj9)

/* Constants Definitons */

// LED connected to D0
const int ledPin = D0;

// Morse code sequences for 0-9 respectively
const char morseLookup[10][8] =
{
  "lllll", "sllll", "sslll", "sssll", "ssssl",
  "sssss", "lssss", "llsss", "lllss", "lllls"
};
const char DASH = 'l'; // Letter that represents a dash
const char DOT = 's'; // Letter that represents a dot

// Duration definitions
const int DURATION_DOT = 300;
const int DURATION_DASH = 3 * DURATION_DOT;
const int DURATION_INTRALETTER_GAP = DURATION_DOT;
const int DURATION_INTERLETTER_GAP = 3 * DURATION_DOT;
const int DURATION_INTERWORD_GAP = 7 * DURATION_DOT;

// Generalization of morse code input
const int CIRCULAR_BUFFER_LENGTH = 256;
char circularBuffer[CIRCULAR_BUFFER_LENGTH];
int start = 0;
int end = 0;

/* The Setup Logic */

void setup()
{
  // Initialize the digital pin as an output:
  pinMode(ledPin, OUTPUT);
  // Begin serial I/O
  Serial.begin(9600);
  // Register function
  Particle.function("morse", cloudMorse);
}

/* The Loop */

void loop()
{
  while (Serial.available())
  {
    // Read a single inputted character
    if (bufferCanPush())
    {
      bufferPush(Serial.read());
    }
  }
  if (bufferCanPop())
  {
    // Get a single inputted character from the buffer
    char readValue = bufferPop();
    // Write it to verify that it was read
    Serial.write(readValue);
    // Get its numeric value
    int numericValue = readValue - '0';
    // Check to see if it's actually a numeric value or not
    if (numericValue >= 0 && numericValue <= 9)
    {
      // Get the length of the morse code sequence corresponding to
      // this value
      int seqLength = strlen(morseLookup[numericValue]);
      for (int i = 0; i < seqLength; i++)
      {
        // Depending on the command letter in this string, decide whether
        // to emit a dot or a dash
        char command = morseLookup[numericValue][i];
        if (command == DOT)
        {
          digitalWrite(ledPin, HIGH);
          delay(DURATION_DOT);
          digitalWrite(ledPin, LOW);
        }
        else if (command == DASH)
        {
          digitalWrite(ledPin, HIGH);
          delay(DURATION_DASH);
          digitalWrite(ledPin, LOW);
        }
        // Depending on whether this is the last emittance in this sequence,
        // delay by either the intraletter or interletter gap
        if (i < seqLength - 1)
        {
          delay(DURATION_INTRALETTER_GAP);
        }
        else
        {
          delay(DURATION_INTERLETTER_GAP);
        }
      }
    }
    else
    {
      // If it's letter, do nothing. But if it's a space, trigger a delay.
      if (readValue == ' ')
      {
        delay(DURATION_INTERWORD_GAP);
      }
    }
  }
}

/* Functions registered online */

int cloudMorse(String arg) {
  for (int i = 0; i < arg.length(); i++)
  {
    if (bufferCanPush())
    {
      bufferPush(arg[i]);
    }
  }
  return 0;
}

/* Functions for manipulating the circular buffer */

// Returns whether the circular buffer can accept another character
bool bufferCanPush()
{
  return end != start - 1 && !(start == 0 && end == CIRCULAR_BUFFER_LENGTH - 1);
}

// Pushes a character into the circular buffer
// Does nothing if it's not possible
void bufferPush(char input)
{
  if (bufferCanPush())
  {
    circularBuffer[end++] = input;
    if (end == CIRCULAR_BUFFER_LENGTH)
    {
      end = 0;
    }
  }
}

// Returns whether a character can be popped from the circular buffer
bool bufferCanPop()
{
  return start != end;
}

// Pops a character frm the circular buffer
// Returns zero if it's not possible
char bufferPop()
{
  if (bufferCanPop())
  {
    char output = circularBuffer[start++];
    if (start == CIRCULAR_BUFFER_LENGTH)
    {
      start = 0;
    }
    return output;
  }
  return 0;
}
