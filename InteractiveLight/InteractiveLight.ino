int SPEED = 128;

const int SCALE_LENGTH = 8;
const float notes[SCALE_LENGTH] = {27.5, 30.87, 16.35, 18.35, 20.60, 21.83, 24.5};

const int MOTOR_GO = D0;
const int MOTOR_FORWARD = D1;
const int MOTOR_BACKWARD = D2;
const int SPEAKER = D3;
const int LED_RED = D4;
const int LED_GREEN = D5;
const int LED_BLUE = D6;

int JOYSTICK_CLICK = A0;
int JOYSTICK_HORIZONTAL = A1;
int JOYSTICK_VERTICAL = A2;

const int PUZZLE_LENGTH = 4;
const int NUM_TRIES_ALLOWED = 10;
const int NUM_TRIES_WARNING = 3;
const char* successPitches[PUZZLE_LENGTH] = { "C5", "G5", "C6", "E6" };
int truth[PUZZLE_LENGTH];
int inputs[PUZZLE_LENGTH];
int inputNumber = 0;
int chosenDirection;
bool directionChosen = false;
int numTriesLeft = NUM_TRIES_ALLOWED;
bool init = true;

const char* seq = "rgbckmywk";

void setup()
{
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(MOTOR_GO, OUTPUT);
  pinMode(MOTOR_FORWARD, OUTPUT);
  pinMode(MOTOR_BACKWARD, OUTPUT);
  pinMode(SPEAKER, OUTPUT);
  pinMode(JOYSTICK_CLICK, INPUT);
  pinMode(JOYSTICK_HORIZONTAL, INPUT);
  pinMode(JOYSTICK_VERTICAL, INPUT);
}

void loop()
{
  // If this flag is set, reset stuff
  if (init)
  {
    init = false;
    numTriesLeft = NUM_TRIES_ALLOWED;
    // generate new sequence
    for (int i = 0; i < PUZZLE_LENGTH; i++)
    {
      int direction = random(0, 8);
      // don't let us choose 4
      if (direction >= 4)
      {
        direction++;
      }
      truth[i] = direction;
    }
  }
  int x = getJoystickX();
  int y = getJoystickY();
  bool clicked = analogRead(JOYSTICK_CLICK) < 16;
  if (clicked)
  {
    Serial.println("Clicked");
  }
  Serial.println(analogRead(JOYSTICK_CLICK));

  /*Serial.print("(");
  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.println(")");*/

  int dirCode = getDirCode(x, y);
  if (clicked)
  {
    inputNumber = 0;
    directionChosen = false;
  }
  if (dirCode != 4)
  {
    if (chosenDirection % 2 == 1 || !directionChosen)
    {
      chosenDirection = dirCode;
    }
    directionChosen = true;
  }
  else if (dirCode == 4 && directionChosen)
  {
    inputs[inputNumber++] = chosenDirection;
    directionChosen = false;
    if (inputNumber == PUZZLE_LENGTH)
    {
      inputNumber = 0;
      int numCorrect = 0;
      for (int i = 0; i < PUZZLE_LENGTH; i++)
      {
        if (truth[i] == inputs[i])
        {
          numCorrect++;
        }
      }
      if (numCorrect == PUZZLE_LENGTH)
      {
        // flash green: winner
        for (int i = 0; i < numCorrect; i++)
        {
          tone(SPEAKER, frequencyFromNote(successPitches[i]), 250);
          setLEDColor('g');
          delay(250);
          setLEDColor('k');
          delay(250);
        }
        setMotorDirection(-1);
        delay(1000);
        setMotorDirection(0);
        delay(5000);
        setMotorDirection(1);
        delay(800);
        setMotorDirection(0);
        init = true;
      }
      else
      {
        if (numCorrect == 0)
        {
          // flash red
          tone(SPEAKER, frequencyFromNote("A2"), 600);
          for (int i = 0; i < 3; i++)
          {
            setLEDColor('r');
            delay(100);
            setLEDColor('k');
            delay(100);
          }
        }
        else
        {
          // flash blue once for each one correct
          for (int i = 0; i < numCorrect; i++)
          {
            tone(SPEAKER, frequencyFromNote(successPitches[i]), 250);
            setLEDColor('b');
            delay(250);
            setLEDColor('k');
            delay(250);
          }
        }
        numTriesLeft--;
        if (numTriesLeft == 0)
        {
          const char* failurePitches[PUZZLE_LENGTH] = { "A4", "E4", "C4", "A3" };
          // flash magenta: game over
          for (int i = 0; i < 4; i++)
          {
            tone(SPEAKER, frequencyFromNote(failurePitches[i]), 250);
            setLEDColor('m');
            delay(250);
            setLEDColor('k');
            delay(250);
          }
          init = true;
        }
        else if (numTriesLeft <= NUM_TRIES_WARNING)
        {
          // flash yellow: tries left
          for (int i = 0; i < numTriesLeft; i++)
          {
            setLEDColor('y');
            delay(250);
            setLEDColor('k');
            delay(250);
          }
        }
      }
    }
  }
}

int getJoystickX()
{
  float amount = -(analogRead(JOYSTICK_HORIZONTAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int getJoystickY()
{
  float amount = (analogRead(JOYSTICK_VERTICAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

void setMotorDirection(int direction)
{
  analogWrite(MOTOR_GO, 0);
  if (direction > 0)
  {
    analogWrite(MOTOR_FORWARD, SPEED);
    analogWrite(MOTOR_BACKWARD, 0);
  }
  else if (direction < 0)
  {
    analogWrite(MOTOR_FORWARD, 0);
    analogWrite(MOTOR_BACKWARD, SPEED);
  }
  else
  {
    analogWrite(MOTOR_FORWARD, 0);
    analogWrite(MOTOR_BACKWARD, 0);
  }
  analogWrite(MOTOR_GO, SPEED);
  Serial.println(direction);
}

int getDirCode(int x, int y)
{
  return (y + 1) * 3 + x + 1;
}

void setLEDColor(char colorCode)
{
  switch (colorCode)
  {
    case 'r':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'g':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'b':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'c':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'm':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'y':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'w':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, HIGH);
      break;
    default:
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, LOW);
      break;
  }
}

// Helper function that turns, for example, A4 -> 440Hz
int frequencyFromNote(const char* note)
{
  if (strlen(note) < 2)
  {
    return 0;
  }
  float base = notes[note[0] - 'A'];
  int octave = note[1] - '0';
  return (int)(base * (1 << octave));
}
