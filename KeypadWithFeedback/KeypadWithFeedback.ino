#define PORTB (GPIOB->ODR)
#define PINC (GPIOC->IDR)
const int row_count = 7;
const int rows[7] = {A3, A4, A5, DAC, WKP, RX, TX};

const int16_t portB_message_mask = 0b0000000011111000;
const int16_t pinC_column1_mask = 0b0000000000000100;
const int16_t pinC_column2_mask = 0b0000000000001000;
const int16_t pinC_column3_mask = 0b0000000000100000;
const byte character_patterns_table[7][3] =
  {{10, 10, 10}, {10, 10, 10}, {10, 10, 10},
   { 1, 2, 3}, { 4, 5, 6}, { 7, 8, 9}, {11, 0, 12}};
const char conversion_table[7][3] =
  {{'!', '!', '!'}, {'!', '!', '!'}, {'!', '!', '!'}, {'1', '2', '3'},
   {'4', '5', '6'}, {'7', '8', '9'}, {'*', '0', '#'}};
const int16_t char_patterns[13][7] =
  {{~0b01110, ~0b10001, ~0b10011, ~0b10101, ~0b11001, ~0b10001, ~0b01110},
   {~0b00100, ~0b01100, ~0b00100, ~0b00100, ~0b00100, ~0b00100, ~0b01110},
   {~0b01110, ~0b10001, ~0b00001, ~0b00110, ~0b01000, ~0b10000, ~0b11111},
   {~0b11111, ~0b00001, ~0b00010, ~0b00110, ~0b00001, ~0b10001, ~0b01110},
   {~0b00010, ~0b00110, ~0b01010, ~0b10010, ~0b11111, ~0b00010, ~0b00010},
   {~0b11111, ~0b10000, ~0b11110, ~0b00001, ~0b00001, ~0b10001, ~0b01110},
   {~0b00111, ~0b01000, ~0b10000, ~0b11110, ~0b10001, ~0b10001, ~0b01110},
   {~0b11111, ~0b00001, ~0b00010, ~0b00100, ~0b01000, ~0b01000, ~0b01000},
   {~0b01110, ~0b10001, ~0b10001, ~0b01110, ~0b10001, ~0b10001, ~0b01110},
   {~0b01110, ~0b10001, ~0b10001, ~0b01111, ~0b00001, ~0b00010, ~0b11100},
   {~0b00100, ~0b00100, ~0b00100, ~0b00100, ~0b00100, ~0b00000, ~0b00100},
   {~0b00100, ~0b10101, ~0b01110, ~0b11111, ~0b01110, ~0b10101, ~0b00100},
   {~0b01010, ~0b01010, ~0b11111, ~0b01010, ~0b11111, ~0b01010, ~0b01010}};
int t;
int i;

void setup()
{
  t = 0;
  i = 0;
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
}

void loop()
{
  for (int r = 0; r < 7; r++)
  {
    clean_update(PORTB, portB_message_mask, char_patterns[i][r] << 3);
    // write to LEDs
    pinMode(rows[r], OUTPUT);
    digitalWrite(rows[r], HIGH);
    delay(1);
    if (r >= 3)
    {
      int c = ((PINC & pinC_column1_mask) ? 1 : 0) +
              ((PINC & pinC_column2_mask) ? 2 : 0) +
              ((PINC & pinC_column3_mask) ? 3 : 0);
        if (c != 0)
        {
          c--; // get it zero-indexed
          int oldi = i;
          i = character_patterns_table[r][c];
          if (i != oldi)
          {
            Serial.print(conversion_table[r][c]);
          }
        }
    }
    digitalWrite(rows[r], LOW);
    pinMode(rows[r], INPUT);
  }
}

// a polite way to update bits in registers
inline void clean_update(__IO uint32_t &target_register, int mask, int value_to_update)
{
  target_register = (target_register & ~mask) | (value_to_update & mask);
}