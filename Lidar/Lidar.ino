/**
 * Lidar.ino - kkj9
 */

#include <Stepper.h>
#include "math.h"

int position = 0;
int i = 0;
Stepper stepper(200, D0, D1, D2, D3);

void setup()
{
  stepper.setSpeed(30);
}

void loop()
{
  if (Serial.available())
  {
    Serial.read(); // Wait until we get something from the SONAR script.
    int val = abs(49 - i);
    stepper.step(val - position);
    position = val;
    int angle = position * 18;
    int dist = analogRead(A0);
    dist = 10 * (70.0 / (dist / 4096.0 * 5) - 6);
    Serial.print(angle);
    Serial.print(",");
    Serial.print(dist);
    Serial.print(".");
    i++;
    if (i == 100)
    {
      i = 0;
    }
  }
}
