import processing.serial.*;

Serial port;
boolean c = true;

void setup()
{
  port = new Serial(this, "COM4", 9600);
  port.bufferUntil('\n');
}

void draw()
{
}

void keyPressed()
{
  if (c)
  {
    if (key == 'z')
    {
      port.write("P 5\n");
      c = false;
    }
    else if (key == 'x')
    {
      port.write("P -30\n");
      c = false;
    }
    println(key);
  }
}

void serialEvent(Serial p)
{
  c = true;
}