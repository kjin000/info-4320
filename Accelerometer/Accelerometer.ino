/**
 * Accelerometer.ino - Kelvin Jin (kkj9)
 */

const byte LSM303D_address = 0b011110;
const byte MULTI_READ = 0b01000000;
const byte CTRL1 = 0x20;
const byte ACC_ALL_AXIS = 0b00000111;
const byte ACC_REFRESH_100Hz = 0b01100000;
const byte CTRL5 = 0x24;
const byte MAG_REFRESH_100Hz = 0b00010100;
const byte TEMP_ON = 0b10000000;
const byte CTRL7 = 0x26;
const byte MAG_CONTINUOUS = 0b00000000;
const byte OUT_X_L_A = 0x28;
const byte OUT_Y_L_A = 0x2A;
const byte OUT_Z_L_A = 0x2C;
const byte OUT_X_L_M = 0x08;
const byte OUT_Y_L_M = 0x0A;
const byte OUT_Z_L_M = 0x0C;
const byte TEMP_OUT_L = 0x05;

const byte READ = 0b10000000;

const int CS_pin = A2;

void setup()
{
  // open the serial port
  Serial.begin(9600);
  // setting up
  pinMode(CS_pin,OUTPUT);
  digitalWrite(CS_pin,HIGH);
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setClockDivider(SPI_CLOCK_DIV32);
  SPI.begin();
  // sending control messages
  digitalWrite(CS_pin,LOW);
  SPI.transfer(CTRL1);
  SPI.transfer(ACC_REFRESH_100Hz | ACC_ALL_AXIS);
  digitalWrite(CS_pin,HIGH);
  digitalWrite(CS_pin,LOW);
  SPI.transfer(CTRL5);
  SPI.transfer(MAG_REFRESH_100Hz);
  digitalWrite(CS_pin,HIGH);
  digitalWrite(CS_pin,LOW);
  SPI.transfer(CTRL7);
  SPI.transfer(MAG_CONTINUOUS);
  digitalWrite(CS_pin,HIGH);
}

void loop()
{
  // acceleration
  int16_t acc[3];
  // magnetism
  int16_t mag[3];
  // read acceleration
  digitalWrite(CS_pin,LOW);
  SPI.transfer(READ | OUT_X_L_A | MULTI_READ);
  for (int i = 0; i < 3; i++)
  {
    acc[i] = SPI.transfer(0);
    acc[i] |= SPI.transfer(0) << 8 ;
  }
  digitalWrite(CS_pin,HIGH);
  // read magnetism
  digitalWrite(CS_pin,LOW);
  SPI.transfer(READ | OUT_X_L_M | MULTI_READ);
  for (int i = 0; i < 3; i++)
  {
    mag[i] = SPI.transfer(0);
    mag[i] |= SPI.transfer(0) << 8 ;
  }
  digitalWrite(CS_pin,HIGH);
  // print
  Serial.print("AX = ");
  Serial.print((2.0*acc[0])/0x7FFF);
  Serial.print(" AY = ");
  Serial.print((2.0*acc[1])/0x7FFF);
  Serial.print(" AZ = ");
  Serial.print((2.0*acc[2])/0x7FFF);
  Serial.print(" MX = ");
  Serial.print((4.0*mag[0])/0x7FFF);
  Serial.print(" MY = ");
  Serial.print((4.0*mag[1])/0x7FFF);
  Serial.print(" MZ = ");
  Serial.println((4.0*mag[2])/0x7FFF);
  // wait a bit
  delay(500);
}
