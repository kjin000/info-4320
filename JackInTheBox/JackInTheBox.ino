int SPEED = 78;

void setup()
{
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
}
void loop()
{
  if (analogRead(A1) > 2000)
  {
    setDirection(1);
  }
  else
  {
    setDirection(0);
  }
}

void setDirection(int direction)
{
  analogWrite(D0, 0);
  if (direction > 0)
  {
    analogWrite(D1, SPEED);
    analogWrite(D2, 0);
  }
  else if (direction < 0)
  {
    analogWrite(D1, 0);
    analogWrite(D2, SPEED);
  }
  else
  {
    analogWrite(D1, 0);
    analogWrite(D2, 0);
  }
  analogWrite(D0, SPEED);
}
