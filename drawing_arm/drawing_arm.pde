import processing.serial.*;
import gab.opencv.*;

static float pixelsPerUnit = 25;
static float stepSize = 0.1f;
static float stepperStep = 1.8f;
static boolean usePort = true;
static boolean waitForResponse = true;
static boolean displayInstructions = true;

// drawShape(int numSides, float radius, int skip = 0)
// drawContour(PApplet parent, String srcImage, int threshold, float polygonLoD, float scale)
// reset()
// setRSpeed(float amount)
// setRAcceleration(float amount)
// setThetaSpeed(float amount)
// setThetaAcceleration(float amount)
// rAxisMovement(float amount)
// thetaAxisMovement(float amount)
// angularMovementAbout(float x, float y, float angle, float direction)
// linearMovement(float xIn, float yIn)
// linearMovementTo(float xIn, float yIn)
// lowerPen()
// raisePen()
// wait(int milliseconds)
void program(DrawingArm drawingArm)
{
  drawingArm.setRSpeed(2000);
  drawingArm.setThetaSpeed(2000);
  drawingArm.linearMovement(0, 1);
  drawingArm.drawContour(this, "2819.png", 70, 1, 0.5f);
  //drawingArm.lowerPen();
  //drawingArm.angularMovementAbout(2, 1, PI, 1);
  //drawingArm.raisePen();
  ////////////////////////////////////
  //drawingArm.setThetaSpeed(1000);
  //drawingArm.rAxisMovement(2);
  //drawingArm.thetaAxisMovement(-PI / 4);
  //drawingArm.lowerPen();
  //drawingArm.thetaAxisMovement(PI / 2);
  //drawingArm.raisePen();
  ////////////////////////////////////
  //drawingArm.lowerPen();
  //drawingArm.linearMovementTo(0, 3);
  //drawingArm.wait(500);
  //drawingArm.linearMovementTo(1, 3);
  //drawingArm.wait(500);
  //drawingArm.linearMovementTo(1, 4);
  //drawingArm.wait(500);
  //drawingArm.linearMovementTo(2, 4);
  //drawingArm.raisePen();
  //drawingArm.linearMovementTo(2, 2);
  //drawingArm.drawShape(3, 1);
  //drawingArm.linearMovementTo(2, 5);
  //drawingArm.drawShape(4, 1);
  //drawingArm.linearMovementTo(-2, 5);
  //drawingArm.drawShape(5, 1, 1);
  //drawingArm.linearMovementTo(-2, 1);
  //drawingArm.lowerPen();
  //drawingArm.angularMovementAbout(-2, 2, 2 * PI, -1);
  //drawingArm.raisePen();
  ////////////////////////////////////
  //for (int i = 1; i <= 3; i++)
  //{
  //  for (int j = -2; j <= 2; j++)
  //  {
  //    drawingArm.linearMovementTo(j, i);
  //    drawingArm.lowerPen();
  //    drawingArm.wait(500);
  //    drawingArm.raisePen();
  //  }
  //}
  //for (int i = 0; i < 10; i++)
  //{
  //  drawingArm.lowerPen();
  //  drawingArm.linearMovement(10 * ((i % 2) * 2 - 1), 0);
  //  drawingArm.raisePen();
  //  drawingArm.linearMovement(0, 0.5f);
  //}
}

// region - setup

// experimentally determined constants
static float penXOffset = 0.3f;
static float baseYOffset = 4.5f;
static float minR = 4.5f;
static float maxR = 15;
static float penUp = -30;
static float penDown = 30;
static float stepperIncrementsPerThetaRadian = 200;
static float stepperIncrementsPerRInch = 120;

static int MODE_RESET = 0;
static int MODE_LIN_MOVEMENT_ABS = 1;
static int MODE_LIN_MOVEMENT_REL = 2;
static int MODE_PEN_POSITION = 3;
static int MODE_R_MOVEMENT_REL = 4;
static int MODE_THETA_MOVEMENT_REL = 5;
static int MODE_WAIT = 6;
static int MODE_ANG_MOVEMENT_REL = 7;
static int MODE_ANG_MOVEMENT_ABS = 8;
static int MODE_SET_VAR = 9;

static boolean _c = true;

PGraphics canvas;
DrawingArm drawingArm;
Pen pen;
Paper paper;
Serial port = null;
float t = 0;

void setup()
{
  size(640, 480);
  canvas = createGraphics(640, 480);
  paper = new Paper(11, 8.5f);
  pen = new Pen(color(0, 0, 0), 1);
  drawingArm = new DrawingArm(pen, paper);
 
  drawingArm.reset();
  program(drawingArm);
  drawingArm.reset();
  if (usePort)
  {
    port = new Serial(this, "COM4", 9600);
  }
  if (port != null)
  {
    port.bufferUntil('\n');
  }
}

void draw()
{
  background(255);
  drawingArm.update(port);
  drawingArm.drawToBuffer(canvas);
  image(canvas, 0, 0);
}

// region - helpers

void serialEvent(Serial p)
{
  if (port == p)
  {
    _c = true;
  }
}

static float length(float x, float y)
{
  return sqrt(x * x + y * y);
}

static float sign(float x)
{
  if (x < 0)
  {
    return -1;
  }
  else if (x > 0)
  {
    return 1;
  }
  return 0;
}

static void send(Serial port, char command, float value)
{
  String toSend = "" + command + " " + value + "\n";
  if (displayInstructions)
  {
    print(toSend);
  }
  if (port != null)
  {
    port.write(toSend);
    _c = false;
  }
}

// region - OOP

class Pen
{
  public color tint;
  public float thickness;
  public float intensity;
  
  public Pen(color tint, float thickness)
  {
    this.tint = tint;
    this.thickness = thickness;
  }
}

class Paper
{
  float w;
  float h;
  PGraphics contents;
  
  public Paper(float w, float h)
  {
    this.w = w;
    this.h = h;
    contents = createGraphics((int)(w * pixelsPerUnit), (int)(h * pixelsPerUnit));
  }
  
  public void mark(Pen pen, float x, float y)
  {
    contents.beginDraw();
    contents.noStroke();
    contents.fill(pen.tint);
    contents.ellipse((x + w / 2) * pixelsPerUnit, y * pixelsPerUnit, pen.thickness, pen.thickness);
    contents.endDraw();
  }
  
  public void mark(Pen pen, float xa, float ya, float xb, float yb)
  {
    contents.beginDraw();
    contents.stroke(pen.tint);
    contents.strokeWeight(pen.thickness);
    contents.line((xa + w / 2) * pixelsPerUnit, ya * pixelsPerUnit, (xb + w / 2) * pixelsPerUnit, yb * pixelsPerUnit);
    contents.noStroke();
    contents.fill(pen.tint);
    contents.ellipse((xa + w / 2) * pixelsPerUnit, ya * pixelsPerUnit, pen.thickness, pen.thickness);
    contents.ellipse((xa + w / 2) * pixelsPerUnit, ya * pixelsPerUnit, pen.thickness, pen.thickness);
    contents.endDraw();
  }
  
  public PGraphics getGraphics()
  {
    return contents;
  }
  
  public float getWidth()
  {
    return w;
  }
  
  public float getHeight()
  {
    return h;
  }
}

class DrawingArm
{
  // target
  FloatList targets;
  // state of the arm
  float x = 0;
  float y = 0;
  float theta;
  float r;
  boolean isPenDown = false;
  // drawing
  float xPrev = 0;
  float yPrev = 0;
  boolean prevValid = false;
  // masks
  PImage mask;
  int previousValue = 0;
  
  Pen pen;
  Paper paper;
  
  public DrawingArm(Pen pen, Paper paper)
  {
    this.pen = pen;
    this.paper = paper;
    targets = new FloatList();
  }
  
  public void drawShape(int numSides, float radius)
  {
    drawShape(numSides, radius, 0);
  }
  
  public void drawShape(int numSides, float radius, int skip)
  {
    float prevX = 0;
    float prevY = 0;
    for (int i = 0; i <= numSides; i++)
    {
      if (i == 1)
      {
        lowerPen();
      }
      float angle = (float)((skip + 1) * i) / numSides * 2 * PI;
      float x = radius * cos(angle);
      float y = radius * sin(angle);
      linearMovement(x - prevX, y - prevY);
      wait(200);
      prevX = x;
      prevY = y;
      if (i == numSides)
      {
        raisePen();
        linearMovement(-x, -y);
      }
    }
  }
  
  public void drawContour(PApplet parent, String srcImage, int threshold, float polygonLoD, float scale)
  {
    PImage src = loadImage(srcImage);
    OpenCV opencv = new OpenCV(parent, src);
    opencv.gray();
    opencv.threshold(threshold);
    // figure out dimensions
    float scaleFactor = min((float)paper.getWidth() / src.width, (float)paper.getHeight() / src.height) * scale;
    float halfWidth = src.width / 2.0f;
    ArrayList<Contour> contours = opencv.findContours();
    println("found " + contours.size() + " contours");
    float prevX = 0, prevY = 0, currX = 0, currY = 0;
    for (int h = 0; h < contours.size() - 1; h++)
    {
      contours.get(h).setPolygonApproximationFactor(polygonLoD);
      ArrayList<PVector> points = contours.get(h).getPolygonApproximation().getPoints();
      for (int i = 0; i <= points.size(); i++)
      {
        if (i == 1)
        {
          lowerPen();
        }
        int index = i % points.size();
        currX = (points.get(index).x - halfWidth) * scaleFactor;
        currY = points.get(index).y * scaleFactor;
        linearMovement(currX - prevX, currY - prevY);
        wait(200);
        prevX = currX;
        prevY = currY;
        if (i == points.size())
        {
          raisePen();
        }
      }
    }
    linearMovement(-currX, -currY);
  }
  
  public void reset()
  {
    targets.append(MODE_RESET);
  }
  
  public void setRSpeed(float amount)
  {
    targets.append(MODE_SET_VAR);
    targets.append((float)'H');
    targets.append(amount);
  }
  
  public void setRAcceleration(float amount)
  {
    targets.append(MODE_SET_VAR);
    targets.append((float)'I');
    targets.append(amount);
  }
  
  public void setThetaSpeed(float amount)
  {
    targets.append(MODE_SET_VAR);
    targets.append((float)'J');
    targets.append(amount);
  }
  
  public void setThetaAcceleration(float amount)
  {
    targets.append(MODE_SET_VAR);
    targets.append((float)'K');
    targets.append(amount);
  }
  
  public void rAxisMovement(float amount)
  {
    targets.append(MODE_R_MOVEMENT_REL); // mode
    targets.append(amount);
  }
  
  public void thetaAxisMovement(float amount)
  {
    targets.append(MODE_THETA_MOVEMENT_REL); // mode
    targets.append(amount);
  }
  
  public void angularMovementAbout(float x, float y, float angle, float direction)
  {
    targets.append(MODE_ANG_MOVEMENT_REL);
    targets.append(x);
    targets.append(y);
    targets.append(angle);
    targets.append(direction); // either -1 or 1
    targets.append(0); // to be filled in later
  }
  
  public void linearMovement(float xIn, float yIn)
  {
    targets.append(MODE_LIN_MOVEMENT_REL); // mode
    targets.append(xIn);
    targets.append(yIn);
  }
  
  public void linearMovementTo(float xIn, float yIn)
  {
    targets.append(MODE_LIN_MOVEMENT_ABS); // mode
    targets.append(xIn);
    targets.append(yIn);
  }
  
  public void lowerPen()
  {
    targets.append(MODE_PEN_POSITION);
    targets.append(1);
  }
  
  public void raisePen()
  {
    targets.append(MODE_PEN_POSITION);
    targets.append(0);
  }
  
  public void wait(int milliseconds)
  {
    targets.append(MODE_WAIT);
    targets.append(milliseconds);
  }
  
  private void recalculateCartesian()
  {
    float xa = r * cos(theta);
    float ya = r * sin(theta);
    float xo = sin(theta) * penXOffset;
    float yo = -cos(theta) * penXOffset;
    x = xa + xo;
    y = ya + yo - baseYOffset;
  }
  
  private void recalculatePolar()
  {
    float offsetY = y + baseYOffset;
    theta = asin(penXOffset / length(x, offsetY)) + atan2(offsetY, x);
    r = sqrt(x * x + offsetY * offsetY - penXOffset * penXOffset);
  }
  
  private void helpDrawLine()
  {
    float prevTheta = theta;
    float prevR = r;
    // get new values for theta and r
    recalculatePolar();
    float deltaTheta = (theta - prevTheta) * stepperIncrementsPerThetaRadian / stepperStep;
    float deltaR = (r - prevR) * stepperIncrementsPerRInch / stepperStep;
    // Here's where it gets sent
    send(port, 'T', deltaTheta);
    send(port, 'R', deltaR);
  }
  
  public void update(Serial port)
  {
    if (port != null && waitForResponse && !_c)
    {
      return;
    }
    while (true)
    {
      if (targets.size() == 0)
      {
        return;
      }
      int mode = floor(targets.get(0));
      if (mode == MODE_RESET)
      {
        targets.remove(0);
        theta = PI / 2;
        r = minR;
        recalculateCartesian();
        send(port, 'X', 0);
        break;
      }
      else if (mode == MODE_LIN_MOVEMENT_ABS)
      {
        float xT = targets.get(1);
        float yT = targets.get(2);
        float distance = length(xT - x, yT - y);
        if (distance < 0.001f)
        {
          targets.remove(0);
          targets.remove(0);
          targets.remove(0);
        }
        else
        {
          x = x + min(stepSize / distance, 1) * (xT - x);
          y = y + min(stepSize / distance, 1) * (yT - y);
          helpDrawLine();
          break;
        }
      }
      else if (mode == MODE_ANG_MOVEMENT_REL)
      {
        targets.set(0, MODE_ANG_MOVEMENT_ABS);
        float cxT = targets.get(1);
        float cyT = targets.get(2);
        float dstAngle = targets.get(3);
        float srcAngle = atan2(y - cyT, x - cxT);
        float direction = targets.get(4);
        dstAngle = srcAngle + dstAngle * direction;
        float r = length(x - cxT, y - cyT);
        targets.set(3, dstAngle);
        targets.set(4, srcAngle);
        targets.set(5, r);
      }
      else if (mode == MODE_ANG_MOVEMENT_ABS)
      {
        float cxT = targets.get(1);
        float cyT = targets.get(2);
        float dstAngle = targets.get(3);
        float srcAngle = targets.get(4);
        float r = targets.get(5);
        float arcLength = abs(dstAngle - srcAngle) * 2 * r * PI;
        if (arcLength < 0.001f)
        {
          targets.remove(0);
          targets.remove(0);
          targets.remove(0);
          targets.remove(0);
          targets.remove(0);
          targets.remove(0);
        }
        else
        {
          srcAngle += min(abs(dstAngle - srcAngle), stepSize / r) * sign(dstAngle - srcAngle);
          x = cxT + r * cos(srcAngle);
          y = cyT + r * sin(srcAngle);
          targets.set(4, srcAngle);
          helpDrawLine();
          break;
        }
      }
      else if (mode == MODE_LIN_MOVEMENT_REL)
      {
        targets.set(0, MODE_LIN_MOVEMENT_ABS);
        targets.set(1, x + targets.get(1));
        targets.set(2, y + targets.get(2));
        recalculateCartesian();
        break;
      }
      else if (mode == MODE_PEN_POSITION)
      {
        isPenDown = targets.get(1) != 0;
        targets.remove(0);
        targets.remove(0);
        send(port, 'P', isPenDown ? penDown : penUp);
        break;
      }
      else if (mode == MODE_R_MOVEMENT_REL)
      {
        float rT = r + targets.get(1);
        if (rT < minR) { rT = minR; }
        if (rT > maxR) { rT = maxR; }
        targets.remove(0);
        targets.remove(0);
        float deltaR = (rT - r) * stepperIncrementsPerRInch / stepperStep;
        r = rT;
        recalculateCartesian();
        send(port, 'R', deltaR);
        break;
      }
      else if (mode == MODE_THETA_MOVEMENT_REL)
      {
        float tT = theta + targets.get(1);
        targets.remove(0);
        targets.remove(0);
        float deltaTheta = (tT - theta) * stepperIncrementsPerThetaRadian / stepperStep;
        theta = tT;
        recalculateCartesian();
        send(port, 'T', deltaTheta);
        break;
      }
      else if (mode == MODE_WAIT)
      {
        float waitTime = targets.get(1);
        targets.remove(0);
        targets.remove(0);
        send(port, 'D', waitTime);
      }
      else if (mode == MODE_SET_VAR)
      {
        char letter = (char)(int)targets.get(1);
        float arg = targets.get(2);
        targets.remove(0);
        targets.remove(0);
        targets.remove(0);
        send(port, letter, arg);
      }
    }
    if (isPenDown)
    {
      if (prevValid)
      {
        paper.mark(pen, xPrev, yPrev, x, y);
      }
      else
      {
        paper.mark(pen, x, y);
      }
    }
    xPrev = x;
    yPrev = y;
    prevValid = true;
  }
  
  public void drawToBuffer(PGraphics canvas)
  {
    float ox = canvas.width / 2;
    float oy = 0;
    float ux = cos(theta) * pixelsPerUnit;
    float uy = sin(theta) * pixelsPerUnit;
    float px = canvas.width / 2 - paper.getGraphics().width / 2;
    float py = baseYOffset * pixelsPerUnit;
    canvas.beginDraw();
    canvas.clear();
    canvas.image(paper.getGraphics(), px, py);
    canvas.stroke(0, 0, 0);
    canvas.strokeWeight(2);
    canvas.noFill();
    canvas.rect(px, py, paper.getGraphics().width, paper.getGraphics().height);
    canvas.line(ox, oy, ox + maxR * ux, oy + maxR * uy);
    canvas.stroke(191, 191, 255);
    canvas.line(ox, oy, ox + r * ux, oy + r * uy);
    canvas.stroke(255, 0, 191);
    canvas.line(ox, oy, ox + minR * ux, oy + minR * uy); //<>//
    canvas.noStroke();
    canvas.fill(pen.tint);
    canvas.ellipse(ox + x * pixelsPerUnit, oy + (y + baseYOffset) * pixelsPerUnit, pen.thickness + 5, pen.thickness + 5);
    canvas.endDraw();
  }
}