// DrawingArm.ino - Controls Theta/R motors for drawing arm
// You need to be connected in the serial monitor.
// First it will print "When calibrated, press enter: "
// To calibrate, set rotation of arm to the middle (so it splits the paper in
// half) and the pen fixture as close to the tower as possible.
// Then you can press enter.
// Afterwards it will ask for x and y coordinates in inches.
// Assuming 8.5 x 11 paper, x range is (-5.5, 5.5) while y range is (0, 8.5).

#include "AccelStepper.h"
#include "MultiStepper.h"
#include <math.h>

int JOYSTICK_CLICK = A0;
int JOYSTICK_HORIZONTAL = A1;
int JOYSTICK_VERTICAL = A2;
int POTENTIOMETER = A3;
int SERVO = A4;
int LED_RED = A5;
int LED_GREEN = A6;
int LED_BLUE = A7;

const int CALIBRATION_MODE = 0;
const int ENTRY_MODE = 1;
const int RUNNING_MODE = 2;

int mode;
float currTheta = 0;
float currR = 0;
float currPen = 0;
float baseTheta = 0;
float baseR = 0;
bool prevJoystickClicked = false;
bool sendAfterComplete = false;

AccelStepper stepperR(AccelStepper::FULL4WIRE, D0, D1, D2, D3);
AccelStepper stepperTheta(AccelStepper::FULL4WIRE, D4, D5, D6, D7);
Servo servo;

void setup()
{
  mode = RUNNING_MODE;
  Serial.setTimeout(60000);
  Serial.begin(9600);
  stepperR.setSpeed(500);
  stepperR.setMaxSpeed(500);
  stepperR.setAcceleration(100);
  stepperTheta.setSpeed(500);
  stepperTheta.setMaxSpeed(500);
  stepperTheta.setAcceleration(100);
  servo.attach(SERVO);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  while (!Serial);
}

void loop()
{
  baseR += -getJoystickY() / 10.0f;
  baseTheta += getJoystickX() / 10.0f;
  currPen += getPotentiometer() / 10.0f;
  if (getJoystickClicked())
  {
    stepperR.stop();
    stepperTheta.stop();
    currR = 0;
    currTheta = 0;
    currPen = 0;
  }
  setLEDColor('k');
  while (Serial.available())
  {
    String s = Serial.readStringUntil('\n');
    char command = getCommand(s);
    float arg = getArgument(s);
    switch (command)
    {
      case 'H':
        setLEDColor('w');
        stepperR.setSpeed(arg);
        stepperR.setMaxSpeed(arg);
        break;
      case 'I':
        setLEDColor('w');
        stepperR.setAcceleration(arg);
        break;
      case 'J':
        setLEDColor('w');
        stepperTheta.setSpeed(arg);
        stepperTheta.setMaxSpeed(arg);
        break;
      case 'K':
        setLEDColor('w');
        stepperTheta.setAcceleration(arg);
        break;
      case 'T':
        setLEDColor('r');
        currTheta += arg;
        break;
      case 'R':
        setLEDColor('g');
        currR += arg;
        break;
      case 'P':
        setLEDColor('b');
        currPen += arg;
        break;
      case 'X':
        setLEDColor('y');
        currTheta = currR = 0;
      case 'D':
        setLEDColor('c');
        delay((int)arg);
    }
    sendAfterComplete = true;
  }
  // correction time
  /*while (baseR + currR < 0) { currR += 200; }
  while (baseR + currR > 200) { currR -= 200; }
  while (baseTheta + currTheta < 0) { currTheta += 200; }
  while (baseTheta + currTheta > 200) { currTheta -= 200; }*/
  /*if (currPen > 179) { currPen = 179; }
  if (currPen < 0) { currPen = 0; }*/
  stepperR.moveTo(baseR + currR);
  stepperTheta.moveTo(baseTheta + currTheta);
  servo.write(currPen);
  stepperR.runSpeedToPosition();
  stepperTheta.runSpeedToPosition();
  if (sendAfterComplete
    && stepperR.distanceToGo() == 0
    && stepperTheta.distanceToGo() == 0)
  {
    setLEDColor('m');
    Serial.print("ack");
    Serial.write("\n");
    sendAfterComplete = false;
  }
  // printState();
}

// A mapping
float applyDeadZone(float num, float deadZone)
{
  if (num < deadZone && num > -deadZone)
  {
    return 0;
  }
  else
  {
    return num;
  }
}

float getPotentiometer()
{
  float amount = (analogRead(POTENTIOMETER) - 2048.0f) / 2048.0f;
  return applyDeadZone(amount, 0.25f);
}

float getJoystickX()
{
  float amount = -(analogRead(JOYSTICK_HORIZONTAL) - 2048.0f) / 2048.0f;
  return applyDeadZone(amount, 0.125f);
}

float getJoystickY()
{
  float amount = (analogRead(JOYSTICK_VERTICAL) - 2048.0f) / 2048.0f;
  return applyDeadZone(amount, 0.125f);
}

void printState()
{
  Serial.print("theta = ");
  Serial.print(baseTheta);
  Serial.print(" + ");
  Serial.print(currTheta);
  Serial.print(" r = ");
  Serial.print(baseR);
  Serial.print(" + ");
  Serial.println(currR);
}

bool getJoystickClicked()
{
  bool result = analogRead(JOYSTICK_CLICK) < 16 && !prevJoystickClicked;
  prevJoystickClicked = result;
  return result;
}

char getCommand(String s)
{
  return s.charAt(0);
}

float getArgument(String s)
{
  float result = s.substring(2).toFloat();
  return result;
}

void setLEDColor(char colorCode)
{
  switch (colorCode)
  {
    case 'r':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'g':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'b':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'c':
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'm':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, HIGH);
      break;
    case 'y':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, LOW);
      break;
    case 'w':
      digitalWrite(LED_RED, HIGH);
      digitalWrite(LED_GREEN, HIGH);
      digitalWrite(LED_BLUE, HIGH);
      break;
    default:
      digitalWrite(LED_RED, LOW);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_BLUE, LOW);
      break;
  }
}
