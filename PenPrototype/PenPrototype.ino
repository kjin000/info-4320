int SPEED = 64;

const int MOTOR_GO = D0;
const int MOTOR_FORWARD = D1;
const int MOTOR_BACKWARD = D2;
const int SERVO = D3;

Servo myServo;
float angle;

int JOYSTICK_CLICK = A0;
int JOYSTICK_HORIZONTAL = A1;
int JOYSTICK_VERTICAL = A2;

void setup()
{
  pinMode(MOTOR_GO, OUTPUT);
  pinMode(MOTOR_FORWARD, OUTPUT);
  pinMode(MOTOR_BACKWARD, OUTPUT);
  pinMode(JOYSTICK_CLICK, INPUT);
  pinMode(JOYSTICK_HORIZONTAL, INPUT);
  pinMode(JOYSTICK_VERTICAL, INPUT);
  myServo.attach(SERVO);
}

void loop()
{
  setMotorDirection(getJoystickX());
  angle = angle + getJoystickY();
  /*angle = 0;
  while (Serial.available())
  {
    char a = Serial.read();
    int n = a - '0';
    if (n >= 0 && n <= 9)
    {
      angle = angle * 10 + n;
    }
  }*/
  myServo.write(0);
  delay(100);
}

int getJoystickX()
{
  float amount = -(analogRead(JOYSTICK_HORIZONTAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int getJoystickY()
{
  float amount = (analogRead(JOYSTICK_VERTICAL) - 2048.0f) / 2048.0f;
  if (amount < -0.5f)
  {
    return -1;
  }
  else if (amount > 0.5f)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

void setMotorDirection(int direction)
{
  analogWrite(MOTOR_GO, 0);
  if (direction > 0)
  {
    analogWrite(MOTOR_FORWARD, SPEED);
    analogWrite(MOTOR_BACKWARD, 0);
  }
  else if (direction < 0)
  {
    analogWrite(MOTOR_FORWARD, 0);
    analogWrite(MOTOR_BACKWARD, SPEED);
  }
  else
  {
    analogWrite(MOTOR_FORWARD, 0);
    analogWrite(MOTOR_BACKWARD, 0);
  }
  analogWrite(MOTOR_GO, SPEED);
}
